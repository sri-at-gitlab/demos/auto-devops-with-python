# Auto DevOps with Python

This repo demonstrates the use of GitLab Auto DevOps for a Python project.

## Tech Stack

- GitLab for SCM, Pipelines, Deployment
- Kubernetes cluster as the deployment target
- Python 3
- Pipenv for sensible dependency management
- Flask

## Known Issues

- TODO Verify if still valid
- Set `TEST_DISABLED` variable to `True` as this project has no tests for now