FROM python:3
WORKDIR /usr/src/app
RUN pip install pipenv
RUN pipenv install
COPY . /usr/src/app
ENV PORT 4000
EXPOSE $PORT
CMD [ "pipenv", "run", "flask", "run", "--host=0.0.0.0", "--port=4000" ]
